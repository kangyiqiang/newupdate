import { Injectable } from '@angular/core';
import {Overlay, OverlayKeyboardDispatcher, OverlayPositionBuilder, ScrollStrategyOptions} from '@angular/cdk/overlay';
import {ComponentFactoryResolver, Inject, Injector, NgZone, Renderer2, RendererFactory2} from '@angular/core';
import {DynamicOverlayContainer} from './dynamic-overlay-container.service';
import {Directionality} from '@angular/cdk/bidi';
import {DOCUMENT} from '@angular/common';

@Injectable({
  providedIn: 'root'
})

export class DynamicOverlay extends Overlay {

  // tslint:disable-next-line:variable-name
  private readonly _dynamicOverlayContainer: DynamicOverlayContainer;
  private renderer: Renderer2;

  constructor(scrollStrategies: ScrollStrategyOptions,
              // tslint:disable-next-line:variable-name
              _overlayContainer: DynamicOverlayContainer,
              // tslint:disable-next-line:variable-name
              _componentFactoryResolver: ComponentFactoryResolver,
              // tslint:disable-next-line:variable-name
              _positionBuilder: OverlayPositionBuilder,
              // tslint:disable-next-line:variable-name
              _keyboardDispatcher: OverlayKeyboardDispatcher,
              // tslint:disable-next-line:variable-name
              _injector: Injector,
              // tslint:disable-next-line:variable-name
              _ngZone: NgZone,
              // tslint:disable-next-line:variable-name
              @Inject(DOCUMENT) _document: any, _directionality: Directionality, rendererFactory: RendererFactory2) {
    // tslint:disable-next-line:max-line-length
    super(scrollStrategies, _overlayContainer, _componentFactoryResolver, _positionBuilder, _keyboardDispatcher, _injector, _ngZone, _document, _directionality);
    this.renderer = rendererFactory.createRenderer(null, null);

    this._dynamicOverlayContainer = _overlayContainer;
  }

  public setContainerElement(containerElement: HTMLElement): void {
    this.renderer.setStyle(containerElement, 'transform', 'translateZ(0)');
    this._dynamicOverlayContainer.setContainerElement(containerElement);
  }
}
