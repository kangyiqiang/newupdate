import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';
import { OverlayModule } from '@angular/cdk/overlay';

import {
  MatButtonModule, MatCardModule, MatDialogModule, MatInputModule, MatTableModule, MatFormFieldModule, MatCheckboxModule,
  MatToolbarModule, MatMenuModule, MatIconModule, MatProgressSpinnerModule, MatGridListModule, MatSelectModule, MatSnackBarModule
} from '@angular/material';
@NgModule({
  imports: [
  CommonModule,
  MatToolbarModule,
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatDialogModule,
  MatTableModule,
  MatMenuModule,
  MatIconModule,
  MatProgressSpinnerModule,
  MatGridListModule,
  MatFormFieldModule,
  MatSelectModule,
  MatCheckboxModule,
  FlexLayoutModule,
  TranslateModule,
  MatSnackBarModule,
  OverlayModule
  ],
  exports: [
  CommonModule,
   MatToolbarModule,
   MatButtonModule,
   MatCardModule,
   MatInputModule,
   MatDialogModule,
   MatTableModule,
   MatMenuModule,
   MatIconModule,
   MatProgressSpinnerModule,
   MatGridListModule,
   MatFormFieldModule,
   MatSelectModule,
   MatCheckboxModule,
   FlexLayoutModule,
   TranslateModule,
   MatSnackBarModule,
   OverlayModule
   ],
})
export class CustomMaterialModule { }
