import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogloginsocialComponent } from './dialogloginsocial.component';

describe('DialogloginsocialComponent', () => {
  let component: DialogloginsocialComponent;
  let fixture: ComponentFixture<DialogloginsocialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogloginsocialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogloginsocialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
