import { Component, OnInit, Inject } from '@angular/core';
import { LoginComponent } from '../login.component';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-dialogloginsocial',
  templateUrl: './dialogloginsocial.component.html',
  styleUrls: ['./dialogloginsocial.component.css']
})
export class DialogloginsocialComponent implements OnInit {
  name: string; email: string; photoUrl: string;

  // public data can use of model inteface or any
  // tslint:disable-next-line:max-line-length
  constructor(public dialogRef: MatDialogRef<LoginComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.name     = data.name;
    this.email    = data.email;
    this.photoUrl = data.photoUrl;
  }

  ngOnInit() {

  }

}
