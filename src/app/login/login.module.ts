import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login.component';

// outside component material
import { CustomMaterialModule} from '.././material.module';
import { DialogloginsocialComponent } from './dialogloginsocial/dialogloginsocial.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  }
];

@NgModule({
  declarations: [
    LoginComponent,
    DialogloginsocialComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    CustomMaterialModule
  ],
  entryComponents: [
    DialogloginsocialComponent
  ]
})
export class LoginModule { }
