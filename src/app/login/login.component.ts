import { Component, OnInit, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// outside module
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import { trigger, transition, useAnimation } from '@angular/animations';
import { zoomInDown, zoomIn } from 'ng-animate';
import { MatSnackBar } from '@angular/material';
import {MatDialog} from '@angular/material/dialog';
import { DialogloginsocialComponent } from './dialogloginsocial/dialogloginsocial.component';
import { ProgressService } from '../progress.service';
import { Overlay } from '@angular/cdk/overlay';
import { ProgressContainerComponent } from '../progress-container/progress-container.component';
import { ComponentPortal } from '@angular/cdk/portal';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [
    trigger('zoomInDown', [transition('* => *', useAnimation(zoomInDown))]),
    trigger('zoomIn', [transition('* => *', useAnimation(zoomIn))]),
  ]
})

export class LoginComponent implements OnInit {
  lang: any; user: SocialUser; zoomInDown: any; zoomIn: any; btnLine: boolean; forgotPassword: boolean; showLoginCard: boolean;
  LoginForm: FormGroup;

  // tslint:disable-next-line:max-line-length
  constructor(private service: ProgressService, private overlay: Overlay, private elRef: ElementRef, public dialog: MatDialog, private formBuilder: FormBuilder, private authService: AuthService, public translate: TranslateService, public snackBar: MatSnackBar) {
    translate.addLangs(['en', 'id', 'kor']);
    translate.setDefaultLang('en');

    const browserLang = translate.getBrowserLang();
    translate.use(browserLang.match(/en|id|kor/) ? browserLang : 'en');

    this.LoginForm = this.formBuilder.group({
      username: ['', [Validators.required]], password: ['', [Validators.required]]
    });
  }

  // function spinner temporary dont use it
  /*showGlobalOverlay() {
    this.overlay.create({
      positionStrategy: this.overlay.position().global().centerHorizontally().centerVertically(),
      hasBackdrop: true
    }).attach(new ComponentPortal(ProgressContainerComponent));
  }

  showSelf() {
    const progressRef = this.service.showProgress(this.elRef);
    setTimeout(() => this.service.detach(progressRef), 5000);
  }*/

  messageSnackbar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
   });
  }

  ngOnInit() {
    this.btnLine = true;
    this.forgotPassword = false;
    this.showLoginCard = false;
    this.authService.authState.subscribe((user) => {
      this.user = user;

      if (this.user !== null) {
        this.openDialog();
      }

    });
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  signInWithLine() {
    this.btnLine = false;
    setTimeout(() => {
      this.btnLine = true;
      this.messageSnackbar('Line temporary cant use it!', 'Retry');
    }, 3000);
  }

  signInWithkakaotalk() {
    const objLoginForm  = this.LoginForm.value;
    const checkPassword = 'm123456';

    if (objLoginForm.username === '' || objLoginForm.password === '') {
      this.messageSnackbar('Please fill all the fields!', 'Retry');
    } else if (objLoginForm.password !== checkPassword) {
      this.forgotPassword = true;
      this.messageSnackbar('password incorrect!', 'Retry');
    } else {
      this.forgotPassword = false;
      this.messageSnackbar('Success!', 'Retry');
    }
  }

  signOut(): void {
    this.authService.signOut();
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogloginsocialComponent, {
      data: {name: this.user.name, email: this.user.email, photoUrl: this.user.photoUrl}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.signOut();
      }
      // console.log(`Dialog result: ${result}`);
    });
  }
}
