import { ElementRef, Injectable } from '@angular/core';
import { ComponentPortal } from '@angular/cdk/portal';
import { timer, Subscription } from 'rxjs';
import { ProgressContainerComponent } from './progress-container/progress-container.component';
import { DynamicOverlay } from './overlay/dynamic-overlay.service';
import { OverlayRef, Overlay } from '@angular/cdk/overlay';

@Injectable()
export class ProgressService {

  constructor(private dynamicOverlay: DynamicOverlay, private overlay: Overlay) { }

  public showProgress(elRef: ElementRef) {
    if (elRef) {
      const result: ProgressRef = { subscription: null, overlayRef: null };
      result.subscription = timer(500)
        .subscribe(() => {
          this.dynamicOverlay.setContainerElement(elRef.nativeElement);
          const positionStrategy = this.overlay.position().global().centerHorizontally().centerVertically();
          result.overlayRef = this.overlay.create({
            positionStrategy,
            hasBackdrop: true
          });
          result.overlayRef.attach(new ComponentPortal(ProgressContainerComponent));
        });
      return result;
    } else {
      return null;
    }
    /*if (elRef) {
      const result: ProgressRef = { subscription: null, overlayRef: null };
      result.subscription = timer(500)
        .subscribe(() => {
          this.dynamicOverlay.setContainerElement(elRef.nativeElement);
          const positionStrategy = this.dynamicOverlay.position().global().centerHorizontally().centerVertically();
          result.overlayRef = this.dynamicOverlay.create({
            positionStrategy,
            hasBackdrop: true
          });
          result.overlayRef.attach(new ComponentPortal(ProgressContainerComponent));
        });
      return result;
    } else {
      return null;
    }*/
  }

  detach(result: ProgressRef) {
    if (result) {
      result.subscription.unsubscribe();
      if (result.overlayRef) {
        result.overlayRef.detach();
      }
    }
  }
}

export declare interface ProgressRef { subscription: Subscription, overlayRef: OverlayRef }
